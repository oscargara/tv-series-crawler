<?php

namespace VideoCrawler;

interface Agent
{
    /**
     * @param Episode $episode
     * @return EpisodeVideo|null
     */
    public function getVideo(Episode $episode);
}
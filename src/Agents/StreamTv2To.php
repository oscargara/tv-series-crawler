<?php

namespace VideoCrawler\Agents;

use \VideoCrawler;
use GuzzleHttp\Client as GuzzleHttpClient;

class StreamTv2To implements VideoCrawler\Agent
{

    /**
     * @var GuzzleHttpClient
     */
    private $httpClient;

    static $HTMLCache = [];

    public function __construct(GuzzleHttpClient $httpClient){
        $this->httpClient = $httpClient;
    }

    public function getVideo(VideoCrawler\Episode $episode){

        //http://stream-tv2.to/watch-halt-catch-fire-online-streaming/
        $seriesNameSlug = str_replace(' and ', ' ', strtolower($episode->getSeriesName()) );
        $seriesNameSlug = str_replace(' ', '-', $seriesNameSlug );
        $url = 'http://stream-tv2.to/watch-' . $seriesNameSlug .'-online-streaming/';

        $html = $this->getHtml($url);

        //http://stream-tv-series.info/2016/10/05/halt-and-catch-fire-s3-e8-you-are-not-safe/

        $seriesNameSlug = str_replace(' ', '-', strtolower($episode->getSeriesName()));
        $seriesNameSlugWithEpisode = $seriesNameSlug . '-s'.$episode->getSeasonNumber().'-e'.$episode->getNumber();

        preg_match_all('#"(http://stream-tv-series.net/.*/'.$seriesNameSlugWithEpisode . '-[^"]*)"#m', $html, $m);

        //https://d2510.thevideo.me:8777/g6jtalqptgoammfvg7ifkgmagbdt4maiydtgugudru4mdu7ko7ne55fgbjh5xz4237b3nzjbufqt4dxsdgg5a4wgsnj7pldswd4nypckqv5bwzowrykukwu2ihj2naeuxbsro26aj4v547zkrtms6lcgacckfwvl4svhg55tapwmalgxffyk4ql6fiyancsalmiczvwm2qroldvcnne6b5u42r4gkwph4gwjbqzxigdbvgozduaxglqrmmq2gzj2e5moifpctoyapav355zs7qqhmsmq/v.mp4?direct=false&ua=1&vt=msv3nqynzmbtcu3uecn44w5senfatafo5domh6xmhd3om7bzkbwxmowlbtiqsxz43t2la7p33unwzphjqdpiqjyg4brqxr7ryfoaskubdivcar7srb2kgusy2qcpxpatr3qvhuqh3dmfpaz3lrmn44z2w7azm2cpjntuufldfrs3zzjf7jjybjqdcp5ss2w5fbynsljniuqkccvaqwqgl4q7mymjkzi4m6juqui

        if (!isset($m[1][0])){
            return null;
        }

        $url = $m[1][0];
        $html = $this->getHtml($url);

        preg_match_all('#"(http://allmyvideos.net/embed-.*.html)"#m', $html, $m);

        if (!isset($m[1][0])){
            return null;
        }

        $url = $m[1][0];
        $html = $this->getHtml($url);

        preg_match_all('#"sources" : (\[[\s\{\}\"\[\]:\/\.\?,0-9a-zA-Z]*),\s*"file#m', $html, $m);

        if (!isset($m[1][0])){
            return null;
        }

        $videosFromPage = json_decode($m[1][0]);

        $videosFromPage =  $this->sortFiles($videosFromPage);

        return new VideoCrawler\EpisodeVideo($videosFromPage[0]->file, 'mp4');
    }


    private function getHtml($url){
        if (!isset(self::$HTMLCache[$url])) {
            $response = $this->httpClient->get($url);
            $html = (string)$response->getBody();
            self::$HTMLCache[$url] = $html;
        } else{
            $html = self::$HTMLCache[$url];
        }

        return $html;
    }

    private function sortFiles($videosFromPage){
        usort($videosFromPage, function($a, $b){
            if ((int)$a->label < (int)$b->label) {
                return 1;
            } else if((int)$a->label == (int)$b->label){
                return 0;
            } else{
                return -1;
            }
        });

        return $videosFromPage;
    }

}
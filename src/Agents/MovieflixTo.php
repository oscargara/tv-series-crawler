<?php

namespace VideoCrawler\Agents;

use \VideoCrawler;
use GuzzleHttp\Client as GuzzleHttpClient;

class MovieflixTo implements VideoCrawler\Agent
{

    /**
     * @var GuzzleHttpClient
     */
    private $httpClient;

    static $HTMLCache = [];

    public function __construct(GuzzleHttpClient $httpClient){
        $this->httpClient = $httpClient;
    }

    public function getVideo(VideoCrawler\Episode $episode){

        //Ex. http://movieflix.to/t/halt-and-catch-fire/season-%d
        $seriesNameSlug = str_replace(' ', '-', strtolower($episode->getSeriesName() ));
        $urlPattern = 'http://movieflix.to/t/' . $seriesNameSlug .'/season-'.$episode->getSeasonNumber().'/';

        $queryParam = '?add_mroot=1&cast=0&crew=0&description=1&postersize=poster&previewsizes=%7B%22preview_grid%22:%22video-block%22,%22preview_list%22:%22big3-index%22%7D&season_list=1&slug=1';

        $url = sprintf($urlPattern, $episode->getSeasonNumber()) . $queryParam;

        $options = [
            'headers'=>[
                'Accept'=> 'application/json'
            ]
        ];

        if (!isset(self::$HTMLCache[$url])) {
            $jsonHtml = $this->httpClient->get($url, $options);
            $json = json_decode($jsonHtml->getBody(), true);
            self::$HTMLCache[$url] = $json;
        } else{
            $json = self::$HTMLCache[$url];
        }

        foreach ($json['episodes'] as $episodeSource) {

            $source = false;
            if(isset( $episodeSource['media'])){
                $source =  isset( $episodeSource['media']['1080']) ?  $episodeSource['media']['1080'] : $episodeSource['media']['720'];
            }

            if ($episodeSource['seq'] != $episode->getNumber() || !$source){
                continue;
            }

            $episodeVideo = new VideoCrawler\EpisodeVideo($source, 'mp4');

            return $episodeVideo;
        }
    }



}
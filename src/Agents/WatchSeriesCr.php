<?php

namespace VideoCrawler\Agents;

use \VideoCrawler;
use GuzzleHttp\Client as GuzzleHttpClient;

class WatchSeriesCr implements VideoCrawler\Agent
{

    /**
     * @var GuzzleHttpClient
     */
    private $httpClient;

    static $HTMLCache = [];

    public function __construct(GuzzleHttpClient $httpClient){
        $this->httpClient = $httpClient;
    }

    public function getVideo(VideoCrawler\Episode $episode){

        //http://watchseries.cr/series/the-simpsons/season/27/episode/22
        $seriesNameSlug = str_replace(' ', '-', strtolower($episode->getSeriesName() ));
        $url = 'http://watchseries.cr/series/' . $seriesNameSlug .'/season/'.$episode->getSeasonNumber() . '/episode/' . $episode->getNumber();

        try{
            $iFrameUrl = $this->getIFrameUrl($url);
            $source = $this->getVideoSource($iFrameUrl);
            return new VideoCrawler\EpisodeVideo($source, 'mp4');
        }catch (\Exception $e) {
            return null;
        }
    }


    private function getIFrameUrl($url){

        if (!isset(self::$HTMLCache[$url])) {
            $response = $this->httpClient->get($url);
            $html = (string)$response->getBody();
            self::$HTMLCache[$url] = $html;
        } else{
            $html = self::$HTMLCache[$url];
        }

        preg_match_all('/iframe id="player" data\-src="([^"]*)"/m', $html, $m);

        if (!isset($m[1][0])) {
            throw new \Exception('Iframe not found');
        }

        return $m[1][0];
    }

    private function getVideoSource($iFrameUrl){

        $urlInfo = parse_url($iFrameUrl);

        $url = \GuzzleHttp\Psr7\parse_query($urlInfo['query'])['url'];

        if (!isset(self::$HTMLCache[$url])) {
            $response = $this->httpClient->get($url);
            $html = (string)$response->getBody();
            self::$HTMLCache[$url] = $html;
        } else{
            $html = self::$HTMLCache[$url];
        }

        preg_match_all('/sources\:(.*),/m', $html, $m);

        if (!isset($m[1][0])) {
            throw new \Exception('Not vieos in page found');
        }

        $json = trim($m[1][0]);

        $json = str_replace('file', '"file"', $json);
        $json = str_replace('label', '"label"', $json);

        $videosFromPage = json_decode($json, false);

        if (!$videosFromPage){
            throw new \Exception('Json encode failed: ' . json_last_error_msg());
        }

        usort($videosFromPage, function($a, $b){
           if ((int)$a->label < (int)$b->label) {
               return 1;
           } else if((int)$a->label == (int)$b->label){
               return 0;
           } else{
                return -1;
           }
        });

        return $videosFromPage[0]->file;
    }

}
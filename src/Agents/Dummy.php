<?php

namespace VideoCrawler\Agents;

use \VideoCrawler;

class Dummy implements VideoCrawler\Agent
{

    /**
     * @param VideoCrawler\Episode $episode
     * @return VideoCrawler\EpisodeVideo
     */
    public function getVideo(VideoCrawler\Episode $episode){

        $episodeVideo = new VideoCrawler\EpisodeVideo('http://media.remax-quebec.com/img/fiche/0352/m11922339-pri01-01.jpg', 'mp4');

        return $episodeVideo;
    }
}
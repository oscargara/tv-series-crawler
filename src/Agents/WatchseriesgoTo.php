<?php

namespace VideoCrawler\Agents;

use \VideoCrawler;
use GuzzleHttp\Client as GuzzleHttpClient;

class WatchseriesgoTo implements VideoCrawler\Agent
{

    /**
     * @var \GuzzleHttp\Cookie\CookieJar
     */
    private $jar = null;

    /**
     * @var GuzzleHttpClient
     */
    private $httpClient;

    static $HTMLCache = [];

    public function __construct(GuzzleHttpClient $httpClient){
        $this->httpClient = $httpClient;
        $this->jar = new \GuzzleHttp\Cookie\CookieJar();
    }

    /**
     * @param VideoCrawler\Episode $episode
     * @return null|VideoCrawler\EpisodeVideo
     */
    public function getVideo(VideoCrawler\Episode $episode){

        //www.watchseriesgo.to/episode/halt_and_catch_fire_s3_e10.html
        $seriesNameSlug = str_replace(' ', '_', strtolower($episode->getSeriesName() ));
        $url = 'http://www.watchseriesgo.to/episode/' . $seriesNameSlug .'_s'.$episode->getSeasonNumber() . '_e' . $episode->getNumber().'.html';

        if (!isset(self::$HTMLCache[$url])) {
            $response = $this->httpClient->get($url, ['cookies'=>$this->jar]);

            $html = (string)$response->getBody();
            self::$HTMLCache[$url] = $html;
        } else{
            $html = self::$HTMLCache[$url];
        }

        try{
            return $this->getVideoFromTheVideoPage($html);
        } catch(\Exception $e) {
            //continue
        }

        return null;
    }


    /**
     * @param string $html
     * @return VideoCrawler\EpisodeVideo
     * @throws \Exception
     */
    private function getVideoFromTheVideoPage($html){
        preg_match_all('#href="(/link/thevideo.me/[0-9]*)"#m', $html, $m);

        if (!isset($m[1][0])) {
            throw new \Exception('Page not found');
        }

        $allVideos = [];
        foreach ($m[1] as $index=>$urlPage) {
            if ($index>1) break;
            try{
                $allVideos = array_merge($allVideos, $this->getVideoFromTheVideoIframe('http://www.watchseriesgo.to' .$urlPage));
            } catch(\Exception $e) {
                //continue
            }
        }

        if (count($allVideos)) {
            $allVideos = $this->sortFiles($allVideos);
            var_dump($this->jar->toArray());

            $episodeVideo =  new VideoCrawler\EpisodeVideo($allVideos[0]->file, 'mp4');

            $cookieStr = [];
            /** @var  \GuzzleHttp\Cookie\SetCookie $cookie */
            foreach ($this->jar->getIterator() as $cookie) {
                $cookieStr[] = (string)$cookie;
            }

            $episodeVideo->setHeaders([
                'Cookies'=> $cookieStr,
                'User-Agent'=> 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/53.0.2785.143 Safari/537.36',
                'Accept'=> '*/*',
                'Referer' => 'http://thevideo.me/embed-34pso2blue1n-850x450.html',
                'X-Requested-With' => 'ShockwaveFlash/23.0.0.185',
                'Connection'=> 'keep-alive'
            ]);

            return $episodeVideo;
        }

        throw new \Exception('Videos not found');
    }

    /**
     * @param $url
     * @return mixed|VideoCrawler\EpisodeVideo
     * @throws \Exception
     */
    private function getVideoFromTheVideoIframe($url){

        if (!isset(self::$HTMLCache[$url])) {
            $response = $this->httpClient->get($url, ['cookies'=>$this->jar]);
            $html = (string)$response->getBody();
            self::$HTMLCache[$url] = $html;
        } else{
            $html = self::$HTMLCache[$url];
        }

        preg_match_all('#iframe .* src="([^"]*)"#m', $html, $m);

        if (!isset($m[1][0])) {
            throw new \Exception('Iframe not found');
        }

        $url = $m[1][0];

        if (!isset(self::$HTMLCache[$url])) {
            $response = $this->httpClient->get($url, ['cookies'=>$this->jar]);
            $html = (string)$response->getBody();
            self::$HTMLCache[$url] = $html;
        } else{
            $html = self::$HTMLCache[$url];
        }

        preg_match_all('#sources:.*(\[[\s\{\}\"\[\]:\/\.\?,0-9a-zA-Z]*),\s*\}\],\s*sharing#m', $html, $m);

        if (!isset($m[1][0])){
            throw new \Exception('File not found');
        }

        $videosFromPage = json_decode($m[1][0]);

        return $videosFromPage;
    }

    /**
     * @param $videosFromPage
     * @return array
     */
    private function sortFiles($videosFromPage){
        usort($videosFromPage, function($a, $b){
            if ((int)$a->label < (int)$b->label) {
                return 1;
            } else if((int)$a->label == (int)$b->label){
                return 0;
            } else{
                return -1;
            }
        });

        return $videosFromPage;
    }
}
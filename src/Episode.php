<?php

namespace VideoCrawler;

interface Episode
{
    /**
     * @return int
     */
    public function getNumber();

    /**
     * @return int
     */
    public function getSeasonNumber();

    /**
     * @return string
     */
    public function getSeriesName();
}
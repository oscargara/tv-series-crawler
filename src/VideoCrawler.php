<?php

namespace VideoCrawler;

class VideoCrawler
{

    const ALLOWED_EXTENSIONS = ['mp4', 'mkv', 'flv'];

    /**
     * @var Agent[]
     */
    private $agents = [];

    public function addAgent(Agent $agent){
        $this->agents[] = $agent;
        return $this;
    }

    /**
     * @param Episode $episode
     * @return EpisodeVideo[]
     */
    public function getVideos(Episode $episode){
        $episodeVideos = [];
        foreach($this->agents as $agentId => $agent) {
            $episodeVideo = $agent->getVideo($episode);
            if ($episodeVideo) {
                $episodeVideo->setAgent($agentId);
                $episodeVideos[] = $episodeVideo;
            }
        }

        return $episodeVideos;
    }
}
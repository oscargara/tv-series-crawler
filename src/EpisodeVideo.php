<?php

namespace VideoCrawler;


class EpisodeVideo
{
    /**
     * @var string
     */
    private $url;

    /**
     * @var string
     */
    private $extension;

    /**
     * @var string
     */
    private $agent;

    /**
     * @var array
     */
    private $headers;

    /**
     * @param string $url
     * @param string $extension
     */
    public function __construct($url, $extension){
        $this->url = $url;
        $this->extension = $extension;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return string
     */
    public function getExtension()
    {
        return $this->extension ? $this->extension : 'mp4';
    }

    /**
     * @param string $extension
     */
    public function setExtension($extension)
    {
        $this->extension = $extension;
    }

    /**
     * @return string
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param string $agent
     */
    public function setAgent($agent)
    {
        $this->agent = $agent;
    }

    /**
     * @return array
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     */
    public function setHeaders($headers)
    {
        $this->headers = $headers;
    }

}
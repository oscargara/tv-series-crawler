<?php

class Episode implements \VideoCrawler\Episode
{
    /**
     * @return int
     */
    public function getNumber(){
        return 9;
    }

    /**
     * @return int
     */
    public function getSeasonNumber(){
        return 3;
    }

    /**
     * @return string
     */
    public function getSeriesName(){
        return 'halt and catch fire';
    }
}
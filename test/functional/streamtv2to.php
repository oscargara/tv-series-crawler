<?php

require __DIR__ . '/../../vendor/autoload.php';
require  __DIR__ . '/Episode.php';

$videoCrawler = new VideoCrawler\VideoCrawler();
$videoCrawler->addAgent(new \VideoCrawler\Agents\StreamTv2To(new GuzzleHttp\Client()));

$videos = $videoCrawler->getVideos(new Episode());

var_dump($videos);
